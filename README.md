# CommitBrowser

CommitBrowser is an app that shows the list of recent commits of [android/platform_build](https://github.com/android/platform_build) repository on Github.com.

![](screenshots/screenshot1.png)

The app is utilizing the MVVM design pattern for the UI with the help of the following libraries:

* Android support libraries
* Architecture Components
* Rxjava
* Retrofit2
* Dagger2
* AutoValue
* Timber
* Glide

### Public Trello Board

The project is managed with Trello in a [public board](https://trello.com/b/VlKcj7ue/github-commit-browser).


### License

Copyright 2017 Nimrod Dayan

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
